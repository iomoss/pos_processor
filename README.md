# pos\_processor

Processes a step/dir dump into positional information.

Input is lines, with 6 values on each line;
```
STEP_X, DIR_X, STEP_Y, DIR_Y, STEP_Z, DIR_Z
```
The output is a GCODE going to that position (non scaled, i.e. position is in steps, not mm).
