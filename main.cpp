#include <cstdlib>
#include <cassert>

#include <string>
#include <iostream>

// Definitions of which pin is where in the line
constexpr int PIN_STEP_X = 0;
constexpr int PIN_DIR_X  = 3;
constexpr int PIN_STEP_Y = 1;
constexpr int PIN_DIR_Y  = 4;
constexpr int PIN_STEP_Z = 2;
constexpr int PIN_DIR_Z  = 5;

int main()
{
    // Continously updated positions
    long posX = 0;
    long posY = 0;
    long posZ = 0;

    // Read STDIN line by line
    std::string line;
    std::string old_line = "000000";

    // Process line by line
    while(std::getline(std::cin, line))
    {
        // The line should be 6 characters now
        //assert(line.length() == 6);

        // Read out variables
        const char stepX = line[PIN_STEP_X];
        const char dirX  = line[PIN_DIR_X];
        const char stepY = line[PIN_STEP_Y];
        const char dirY  = line[PIN_DIR_Y];
        const char stepZ = line[PIN_STEP_Z];
        const char dirZ  = line[PIN_DIR_Z];

        // Read out variables
        const char old_stepX = old_line[PIN_STEP_X];
        const char old_stepY = old_line[PIN_STEP_Y];
        const char old_stepZ = old_line[PIN_STEP_Z];

        bool changed = false;
        // Check if a step has occured
        if(stepX == '1' && old_stepX == '0')
        {
            // If it has, move the 'dir' direction
            posX += (dirX == '0' ? -1 : 1);
            changed = true;
        }
        // Same as above, but for Y
        if(stepY == '1' && old_stepY == '0')
        {
            posY += (dirY == '0' ? -1 : 1);
            changed = true;
        }
        // Same as above, but for Z
        if(stepZ == '1' && old_stepZ == '0')
        {
            posZ += (dirZ == '0' ? -1 : 1);
            changed = true;
        }

        if(changed)
        {
            // Write out the line changing
            //std::cout << old_line << " --> " << line << std::endl;
            // Output as human readable
            //std::cout << "X: " << posX << "\tY: " << posY << "\tZ: " << posZ << std::endl;
            // Output as a GCODE
            std::cout << "G0 X" << posX << " Y" << posY << " Z" << posZ << std::endl;
        }

        // Save this line as the old one
        old_line = line;
    }
    return EXIT_SUCCESS;
}
